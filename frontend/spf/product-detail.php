<!doctype html>
<html>

<head>
    <?php require('inc_head.php');
    $pageName = "product"; ?>
</head>

<body>

    <?php require('inc_menu.php'); ?>

    <div class="detail-product">
        <?php require('inc_menu_left.php'); ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="topic-head">BreathCareII Auto CPAP / Bi-Level</div>
                    <p class="model">YH-450</p>

                    <section class="wrap_banner wow fadeInDown">
                        <div class="owl-productSlide owl-carousel owl-theme">
                            <div class="items">
                                <figure class="show-desktop"><img src="images/detail-pd-01.jpg" alt=""></figure>
                            </div>
                            <div class="items">
                                <figure class="show-desktop"><img src="images/detail-pd-01.jpg" alt=""></figure>
                            </div>
                            <div class="items">
                                <figure class="show-desktop"><img src="images/detail-pd-01.jpg" alt=""></figure>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-pd wow fadeInLeft">
                        <div class="specification">
                            <h5>Specification</h5>
                            <p>Mode: CPAP / Auto CPAP</p>
                            <p>Pressure: 4 - 20cm H2O</p>
                            <p>Efficient humidifier / Smart Start / Stop / FPS-Tech /</p>
                            <p>Leak Reminder / Altitude Adjustment / Leak Compensation /</p>
                            <p>Data Management / Central Apnea Detection / Smart Humidification</p>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="download">
                        <a href="#"><i class="bi bi-download"></i> Breathcare Management System</a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="download">
                        <a href="#"><i class="bi bi-download"></i> Breathcare Management System</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require('inc_footer.php'); ?>
    <?php require('scriptjs.php'); ?>

</body>

</html>