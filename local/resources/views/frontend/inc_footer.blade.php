<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5 info">
                <h3>Smart Parking Family</h3>
                <p> location </p>
                <p class="mb-1"><b>Working hours :</b> 8AM – 5PM</p>
                <p>Tel : 02 090 2330</p>
            </div>
            <div class="col-12 col-md-5">
                <div class="row">
                    <div class="col-6">
                        <div class="link-page">
                            <h4>ABOUT US</h4>
                            <ul>
                                <li><a href="{{ url('/about') }}"> VISION </a></li>
                                <li><a href="{{ url('/about') }}"> MISSION </a></li>
                            </ul>
                        </div>
                        {{-- <div class="link-page">
                            <h4>SECTOR</h4>
                            <ul>
                                <li><a href="industry.php">Esaote, Italy</a></li>
                                <li><a href="industry.php">Health industry ecology</a></li>
                                <li><a href="industry.php">Health industry and finance center</a></li>
                            </ul>
                        </div> --}}
                    </div>
                    <div class="col-6">
                        <div class="link-page">
                            <h4>PRODUCT</h4>
                            <ul>
                                <li><a href="{{ url('/product') }}">News Focus</a></li>
                                <li><a href="{{ url('/product') }}">Video Center</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-2">
                <div class="qrCode"><img src="{{ asset('frontend/spf/images/null.jpg') }}" width="80%" alt=""></div>
            </div>
        </div>
    </div>
</footer>
<div class="footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="copyright">Copyright © 2024 --- | All rights reserved.</div>
            </div>
            <div class="col-12 col-md-6 mobile-center">
                <ul class="social">
                    <li><a href="#"><i class="fa-brands fa-facebook-f"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-linkedin-in"></i></a></li>
                    <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>