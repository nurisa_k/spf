<!doctype html>
<html>

<head>
    @include('frontend.inc_head')
    <?php $pageName = "home"; ?>
</head>

<body>
        @include('frontend.inc_menu') 

    <section class="wrap_banner wow fadeInDown">
        <div class="owl-bannerSlide owl-carousel owl-theme">
            <div class="items">
                <figure class="show-desktop"><img src="{{ asset('frontend/spf/images/22674.jpg') }}" alt=""></figure>
                <!-- <figure class="show-mobile"><img src="images/banner-mb.jpg" alt=""></figure> -->
            </div>
            <div class="items">
                <figure class="show-desktop"><img src="{{ asset('frontend/spf/images/22674.jpg') }}" alt=""></figure>
                <!-- <figure class="show-mobile"><img src="images/banner-mb.jpg" alt=""></figure> -->
            </div>
            <div class="items">
                <figure class="show-desktop"><img src="{{ asset('frontend/spf/images/22674.jpg') }}" alt=""></figure>
                <!-- <figure class="show-mobile"><img src="images/banner-mb.jpg" alt=""></figure> -->
            </div>
        </div>
    </section>
    <section class="warp_welcome">
        <div class="container">
            <div class="row">
                <div class="col-12 pad-bottom wow fadeInDown">
                    <div class="topic-head text-center">บริษัท มาสเตอร์ แมคคาโทรนิคส์ จำกัด <span>(SPF)</span></div>
                    <div class="topic-sub"> 
                            ได้ก่อตั้งขึ้นเมื่อปี พ.ศ. 2545 จากการรวมตวของทืมวิศวกร ผู้มีประสบการณ์ทางด้านระบบขนถ่ายวัสดุ โดยเน้นในเรื่อง รอก,เครน และลิฟต์
                            ตลอดระยะเวลาที่ผ่านมา บริษัทฯ ได้ปรับปรุงและ พัฒนาองค์กรอยู่ตลอดเวลา ทั้งด้านคุณภาพสินค้า และการบริการหลังการขายจนปัจจุบัน 
                            บริษัทฯได้รับความไว้วางใจในแวดวงอุตสาหกรรม โดยมการอกแบบและติดตั้งเครนและลิฟต์กว่า 900 ตัว
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section class="warp_products">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="topic-head text-center wow fadeInDown">Products <span>Solutions</span></div>
                    <div class="row">
                        <div class="col-12 col-md-3 wow fadeInDown">
                            <a href="product.php" class="item">
                                <h2>RESPIRATORY</h2>
                                <img src="{{ asset('frontend/spf/images/img-product-01.jpg') }}">
                                <div class="caption">
                                    <p>view more</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3 wow fadeInDown">
                            <a href="#" class="item">
                                <h2>BLOOD PRESSURE </h2>
                                <img src="{{ asset('frontend/spf/images/img-product-02.jpg') }}">
                                <div class="caption">
                                    <p>view more</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3 wow fadeInDown">
                            <a href="#" class="item">
                                <h2>BLOOD GLUCOSE</h2>
                                <img src="{{ asset('frontend/spf/images/img-product-03.jpg') }}">
                                <div class="caption">
                                    <p>view more</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3 wow fadeInDown">
                            <a href="#" class="item">
                                <h2>THERMOMETER</h2>
                                <img src="{{ asset('frontend/spf/images/img-product-04.jpg') }}">
                                <div class="caption">
                                    <p>view more</p>
                                </div>
                            </a>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- <section class="warp_news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="header wow fadeInDown">
                        <div class="topic-head">latest <span>news</span></div>
                        <div class="view-all"><a href="#" class="btn-default btn-red">View All</a></div>
                    </div>
                    <div class="row wow fadeInDown">
                        <div class="col-12 col-md-4">
                            <div class="items">
                                <a href="#">
                                    <figure><img src="{{ asset('frontend/spf/images/news-01.jpg') }}" alt=""></figure>
                                </a>
                                <div class="item-dateShare">
                                    <div><i class="bi bi-calendar3"></i> 18.05.2021</div>
                                </div>
                                <div>
                                    <a href="#">
                                        <div class="latest two-line">
                                            Oxygen concentrator has passed CE with zero defects
                                        </div>
                                        <a href="#" class="btn-readMore-line">read more</a>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="items">
                                <a href="#">
                                    <figure><img src="{{ asset('frontend/spf/images/news-02.jpg') }}" alt=""></figure>
                                </a>
                                <div class="item-dateShare">
                                    <div><i class="bi bi-calendar3"></i> 04.03.2021</div>
                                </div>
                                <div>
                                    <a href="#">
                                        <div class="latest two-line">
                                            Yuwell Acquired the National L
                                            aboratory Certification
                                        </div>
                                        <a href="#" class="btn-readMore-line">read more</a>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="items">
                                <a href="#">
                                    <figure><img src="{{ asset('frontend/spf/images/news-03.jpg') }}" alt=""></figure>
                                </a>
                                <div class="item-dateShare">
                                    <div><i class="bi bi-calendar3"></i> 05.12.2020</div>
                                </div>
                                <div>
                                    <a href="#">
                                        <div class="latest two-line">
                                            New Product Release of Yuwell in
                                            2021
                                        </div>
                                        <a href="#" class="btn-readMore-line">read more</a>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}

    @include('frontend.inc_footer') 
    @include('frontend.scriptjs')  

</body>

</html>